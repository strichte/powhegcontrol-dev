#!/usr/bin/env python2
'''
A convenience script for running Powheg less painfully
Written by and for Stefan Richter to work on the DESY NAF --- your mileage may vary

Written compatible with Python 2 because it needs to run in an ATLAS setup, but should be valid Python 3 as well.
'''
import argparse
from contextlib import contextmanager
import os
import shutil
import textwrap
import time
from datetime import timedelta



@contextmanager
def cd(newdir):
    '''
    Defines a context to change to one directory and, after the context ends, back to the previous directory.

    Example:
    # in directory 'foo/'
    with cd('bar/'):
        do_stuff_in_bar()
    # back in 'foo/'

    Using a context provides some safety from failure.
    '''
    prevdir = os.getcwd()
    os.chdir(os.path.expanduser(newdir))
    try:
        yield
    finally:
        os.chdir(prevdir)



class AtlasSetupError(Exception):
    '''
    Error raised if the ATLAS and PowhegControl setup doesn't look proper
    '''
    pass



def check_ATLAS_setup():
    '''
    Check that the ATLAS setup looks like it's adequate for testing a local PowhegControl version
    '''
    if not 'POWHEGPATH' in os.environ.keys():
        raise AtlasSetupError('POWHEGPATH does not appear to be set')
    # Check that the USER name is in the first element of the PYTHONPATH
    # to make sure that a local
    if not os.environ['USER'] in os.environ['PYTHONPATH'].split(':')[0]:
        raise AtlasSetupError('PYTHONPATH does not seem to point to local PowhegControl version')



def set_up_run_directory(args):
    '''
    If necessary, (re)create the run directory and copy the job option into it
    '''
    if os.path.isdir(args.directory) and not args.keep:
        print('Cleaning existing run directory "{rundir}" (set flag --keep/-k to reuse it)'.format(rundir=args.directory))
        shutil.rmtree(args.directory)
    if not os.path.isdir(args.directory):
        os.mkdir(args.directory)
    else:
        print('Reusing existing run directory "{rundir}" (remove flag --keep/-k to recreate it)'.format(rundir=args.directory))
    shutil.copy(args.joboption, args.directory)
    if args.external_powheg_runcard:
        shutil.copy(args.external_powheg_runcard, os.path.join(args.directory, 'powheg.input'))



def modify_for_external_runcard(joboption):
    print('Modifying copied job option file "{jo}" to use external run card'.format(jo=joboption))
    with open(joboption, 'r') as input_jo:
        with open('temporary_jo.tmp', 'w') as final_jo:
            for original_line in input_jo:
                final_jo.write(original_line.replace('PowhegConfig.generate()', 'PowhegConfig.generate(use_external_run_card=True)'))
    shutil.move('temporary_jo.tmp', joboption)



def modify_for_quick_run(joboption, custom_variations=False):
    print('Modifying copied job option file "{jo}" for a quick run'.format(jo=joboption))
    pdf = 'PowhegConfig.PDF = [260000, 260001]' if custom_variations else ''
    muf = 'PowhegConfig.mu_F = [1.0, 0.5]' if custom_variations else ''
    mur = 'PowhegConfig.mu_R = [1.0, 2.0]' if custom_variations else ''
    quick_options = '''
        PowhegConfig.foldcsi = 1
        PowhegConfig.foldphi = 1
        PowhegConfig.foldy = 1
        PowhegConfig.itmx1 = 1
        PowhegConfig.itmx2 = 1
        PowhegConfig.ncall1 = 2
        PowhegConfig.ncall2 = 2
        PowhegConfig.nubound = 2
        {pdf}
        {muf}
        {mur}
        '''.format(pdf=pdf, muf=muf, mur=mur)
    with open(joboption, 'r') as input_jo:
        with open('temporary_jo.tmp', 'w') as final_jo:
            for original_line in input_jo:
                if 'PowhegConfig.generate' in original_line:
                    # Insert the new lines just before generate() is called
                    # to make sure they take effect:
                    final_jo.write(textwrap.dedent(quick_options))
                final_jo.write(original_line)
    shutil.move('temporary_jo.tmp', joboption)



def compile_athena_command(args):
    program = 'Generate_tf.py'
    opts = {
        '--ecmEnergy' : '13000',
        '--runNumber' : os.path.basename(args.joboption).split('.')[1],
        '--firstEvent' : '1',
        '--randomSeed' : '1',
        '--outputEVNTFile' : 'Powheg.EVNT.root',
        '--maxEvents' : str(args.events),
        '--jobConfig' : args.joboption,
    }
    return program + ''.join([' ' + key + ' ' + val for key, val in opts.items()]) + ' ' + args.athena_args





def main():
    parser = argparse.ArgumentParser(description='Run an Athena MC job option conveniently. Designed to help with development of PowhegControl, but can in principle run any job option you want.', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('joboption', help='Job option to run.')
    parser.add_argument('-d', '--directory', help='Run directory in which the event generation is run. This is useful for avoiding polluting the current working directory with all the junk that is written out.', default='test', type=str)
    parser.add_argument('-k', '--keep', help='If set, do not clean an existing run directory before running. This means that existing integration grids may be used etc.', default=False, action='store_true')
    parser.add_argument('-n', '--events', help='Number of events to generate.', default=10, type=int)
    parser.add_argument('-q', '--quick', help='Try to do a quick run for technical testing. THE RESULTS WILL NOT HAVE ENOUGH QUALITY TO DO PHYSICS. The phase space integration will be extremely coarse and only few PDF and QCD scale variations will be performed.', default=False, action='store_true')
    parser.add_argument('-w', '--no-multiweights', help='Turn off PDF and QCD scale reweighting. This may be necessary e.g. if you want to run POWHEG-BOX-V1 processes with the --quick/-q option (which sets up a few variations).', default=False, action='store_true')
    parser.add_argument('-c', '--condor', help='Run integration highly parallelised on Condor cluster. Only tested on DESY BIRD, your mileage may vary.', default=False, action='store_true')
    parser.add_argument('-a', '--athena-args', help='Arbitrary additional arguments to be passed to the Athena (Generate_tf.py) invocation. Given as a single string between quotes.', default='', type=str)
    parser.add_argument('-D', '--dry-run', help='If set, only set up a run directory and print the Athena command that would have been run, but do not actually execute it.', default=False, action='store_true')
    parser.add_argument('-e', '--external-powheg-runcard', help='If given, use provided the native Powheg run card. This completely bypasses the generator configuration that PowhegControl provides. The run card will be copied to the run directory and given the file name "powheg.input", so that Powheg will find it.', default='', type=str)
    args = parser.parse_args()

    start = time.time()
    print('Starting execution at', time.ctime())

    try:
        check_ATLAS_setup()

        if args.external_powheg_runcard:
            print('Will use native Powheg run card rather than PowhegControl interface')

        set_up_run_directory(args)

        if args.condor:
            raise NotImplementedError('TODO: implement integrating con Condor cluster using the integration grid maker')

        if args.external_powheg_runcard:
            # Leaves the input job option unchanged, just modifies its copy in the run directory
            modify_for_external_runcard(os.path.join(args.directory, args.joboption))
        elif args.quick:
            # Leaves the input job option unchanged, just modifies its copy in the run directory
            # `custom_variations` could be made changeable via a command line argument in the future
            modify_for_quick_run(os.path.join(args.directory, args.joboption), custom_variations=(not args.no_multiweights))

        athena_command = compile_athena_command(args)
        if args.dry_run:
            print('Would execute the following Athena command (inside directory "{rundir}"):'.format(rundir=args.directory))
            print(athena_command)
            print('Remove flag --dry-run/-D to execute it')
        else:
            print('Executing the following Athena command (inside directory "{rundir}"):'.format(rundir=args.directory))
            print(athena_command)
            time.sleep(3) # wait for three seconds to allow the user to check the command
            with cd(args.directory):
                os.system(athena_command)

    finally:
        print('Finishing execution at', time.ctime())
        print('Running time was {0} [h:min:sec:ms]'.format(timedelta(seconds=time.time()-start)))





if __name__ == '__main__':
	main()
